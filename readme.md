# What's App word cloud

Curious about the most used words, how many messages per period of the day, day of week and much more? **Look no further**.

Export a conversation from What's App and feed it to the script `wordcloud.py`.

`python wordcloud.py chat.txt`

### To-Do:
* Message count per 
	* period of the day
	* weekday

* Per person
	* Top 10 must used words
	* Average message length
	* Average word length
	* Total questions(aprox)
	* Total message
	* Total words
	* Total characters