#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os
import re
from optparse import OptionParser
from calendar import month_abbr
from locale import setlocale, LC_ALL
from datetime import datetime

import codecs
import logging

# Logger
logger = logging.getLogger('chat_analyzer')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# File log handler
file_handler = logging.FileHandler('chat_analyzer.log')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
# Console log handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.ERROR)
console_handler.setFormatter(formatter)
# Register the handlers
logger.addHandler(file_handler)
logger.addHandler(console_handler)


class AuthorStats:
	def __init__(self):
		self.top_ten_words = []
		self.n_messages = 0
		self.n_words = 0
		self.n_chars = 0
		self.n_questions = 0
		self.avg_length = 0

		self.DAWN='dawn'
		self.MORNING='morning'
		self.AFTERNOON='afternoon'
		self.NIGHT='night'

		# weekday (0 = Monday)
		self.msg_weekday_time = {
			0: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
			1: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
			2: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
			3: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
			4: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
			5: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
			6: {self.DAWN: 0, self.MORNING: 0, self.AFTERNOON: 0, self.NIGHT: 0},
		}

	def calc_avg_length(self):
		if self.n_messages > 0:
			self.avg_length = self.n_chars / self.n_messages

# Create tokens
class WordCloudGenerator:

	def __init__(self, file_path):
		if not self._check_input_file(file_path):
			logger.error("File '%s' does not exist or isn't a file." % file_path)
			exit(1);

		self.__MIN_TOKEN_LENGTH = 3
		self.__NUMBER_OF_CHARS_TO_OFFSET = 25
		self.__MESSAGE_REGULAR_EXPRESSION_NUMBER_OF_GROUPS = 4
		self.__MESSAGE_REGULAR_EXPRESSION_OLD = r"(\d{1,2})(?:....)(...)(?:....)?(\d{4})?\s(\d{2}):(\d{2})(?:\s-\s)([A-Za-záàâãéèêíïóôõöúçñ ]+)(?::\s)(.*)"
		self.__MESSAGE_REGULAR_EXPRESSION = "(\d{2}\/\d{2}\/\d{4})(?:,\s)(\d{2}:\d{2})(?:\s-\s)([A-Za-záàâãéèêíïóôõöúçñ ]+)(?::\s)(.*)"
		# Emoticon character range based of http://www.easyapns.com/tag/emoticons - http://apps.timwhitlock.info/emoji/tables/unicode#block-2-dingbats
		# Emoticons ( 1F601 - 1F64F )
		# Dingbats ( 2702 - 27B0 )
		# Transport and map symbols ( 1F680 - 1F6C0 )
		# Enclosed characters ( 24C2 - 1F251 )
		self.__EMOTICON_UNICODE_RANGE_REGEX = r'[\ue001-\ue537]'

		self.__IGNORED_CHARS = (
			'.', ',',
			'!', '?',
			'"', '\'',
			'*',
			'(',')'
		)

		self.__IGNORED_WORDS = ('que','bem', 'bom', 'dia', '<mídia', 'omitida>')

		# Reset locale to current locale
		setlocale(LC_ALL, '')
		# Generate a rever dictionary<Month abbreviation, month number>
		self.__dict_month_number = dict((v.lower(),k) for k,v in enumerate(month_abbr))

		self.__output_file_path = ''
		self.__input_file_path = file_path

		# Dictionary<Author, Dictionary<Word, Count>>
		self.__tokens_for = {}

		# Dictionary<Author, [<List of message sent time]>
		self.__user_sent_message_times = {}

		# Dictionary<Author, AuthorStats>
		self.__user_stats = {}

	def process(self):
		'''
		Read input file
		Generate tokens
		Return a Dictionary<Authors Name, Dictionary<Word, Count>>
		'''
		self._process_file_content(self.__input_file_path)
		self.__remove_rare_tokens()

		#return self.__tokens_for

	def __remove_rare_tokens(self):
		to_be_removed = []
		
		# Find rare values
		for author in self.__tokens_for:
			for text in self.__tokens_for[author]:
				if self.__tokens_for[author][text] < 100:
					to_be_removed.append(text)
					continue

				# remover datas
				match = re.match(r'(\d{2}\/\d{2}\/\d{4})', text)
				if match:
					to_be_removed.append(text)
					continue

				match = re.match(r'\d{4}', text)
				if match:
					to_be_removed.append(text)

			# Remove them
			for item in to_be_removed:
				if item in self.__tokens_for[author]:
					del self.__tokens_for[author][item]

	def _check_input_file(self, file_path):
		is_file_ok = False
		do_exist = os.path.exists(file_path)
		is_file = os.path.isfile(file_path)
		
		if do_exist and is_file:
			is_file_ok = True
		
		return is_file_ok

	def _check_output_file(self, file_path):
		if is_file_ok:
			# check if target file is free to use
			if self._is_file_open(file_path):
				is_file_ok = False
				logger.warn("File '%s' is open, please close it." % file_path)

	def _is_file_open(self, file_path):
		is_open = False
		try:
			f = open(file_path, 'w')
			f.close()
		except IOError:
			is_open = True
		return is_open

	def _process_file_content(self, file_path):
		'''
		Iterate through lines
		'''
		line_count = 1
		try:
			with codecs.open(file_path, 'r', 'utf8') as f:
				for line in f:
					logger.debug('#%d' % line_count)
					line = line.lower()
					line = line.strip()

					# 0: DateTime, 1: Author, 2: Message text
					line_data = self._apply_regex(line)
					if not line_data:
						return None

					msg_date_time = line_data[0]
					author = line_data[1]
					msg_text = line_data[2]
					self.stats(msg_date_time, author, msg_text)

					msg_text = self._sanitize(msg_text)
					self._tokenizer(author, msg_text)
					line_count += 1

				# Finish statistics
				ustats = self.__user_stats[author]
				ustats._n_words = len(self.__tokens_for[author])

		except IOError as err:
			logger.error('Couldn\'t open or read file %s. Reason %s' % (file_path, err.strerror))
			exit(3)

	def _apply_regex(self, line):
		'''
		Breaks the line into Date, Authors name, message text
		Returns a tuple
		'''
		regx = re.search(self.__MESSAGE_REGULAR_EXPRESSION, line)
		if regx:
			logger.debug('group size: %d' % len(regx.groups()))
			if len(regx.groups()) == self.__MESSAGE_REGULAR_EXPRESSION_NUMBER_OF_GROUPS:
				date = regx.group(1).split('/')
				day = date[0]
				month = self.__dict_month_number.get(int(date[1]), 1)
				year = date[2]
				# If year is empty, it means that it is the year the export was generated - DEPRECATED
				if not year:
					year = datetime.now().year

				time = regx.group(2).split(':')
				hour = time[0]
				minute = time[1]

				# Parse datetime Y,M,D,H,M
				date = datetime(int(year), int(month), int(day), int(hour), int(minute))

				author = regx.group(3)
				msg_text = regx.group(4)

				return (date, author, msg_text)
			
			else:
				logger.warn('Too few groups - skipping')
		else:
			logger.warn('Unrecognized string - skipping.')

		return None

	def _tokenizer(self, author, msg_text):
		try:
			tokens = msg_text.lower().split(' ')
			
			if author not in self.__tokens_for:
				logger.info('Adding author %s' % author)
				self.__tokens_for[author] = {}

			for token in tokens:
				# Ignore short tokens
				if len(token) < self.__MIN_TOKEN_LENGTH:
					continue

				# Remove ignored words
				if token in self.__IGNORED_WORDS:
					continue

				if token in self.__tokens_for[author]:
					self.__tokens_for[author][token] = self.__tokens_for[author][token] + 1
				else:
					self.__tokens_for[author][token] = 1

		except RuntimeError as err:
			logger.error(err.strerror)

	def _remove_emoticons(self, line):
		'''
		Strip line from emoticons so it won't break when printed
		'''
		emotes = re.findall(self.__EMOTICON_UNICODE_RANGE_REGEX, line)
		logger.debug('%d emoticons found!' % len(emotes))
		for e in emotes:
			line = line.replace(e,'')

		return line

	def _sanitize(self, text):
		'''
		Remove any char from the text that is listed in self.__IGNORED_CHARS
		'''
		#line = self._remove_emoticons(text)
		# TODO: Might be a better way to do this, more performatic maybe
		for char in text:
			if char.encode('utf-8') in self.__IGNORED_CHARS:
				text = text.replace(char, '')
		return text

	def export(self, output_file_path):
		'''
		Output a csv file with the format "author, count, token"
		'''
		try:
			with open(output_file_path, 'w', encoding='utf8') as f:
				f.write('author,count,token\n')
				for author in self.__tokens_for:
					logger.info('Exporting messages from author %s' % author)
					for text in self.__tokens_for[author]:
						txt = ('%s,%d,%s\n' % (author, self.__tokens_for[author][text], text)).strip(' ')
						if len(txt) > 0:
							f.write(txt)
		except IOError as err:
			logger.error('Couldn\'t open or read file. Reason %s' % err.strerror)
			exit(3)

	def stats(self, msg_date, author, msg_text):
		if author not in self.__user_stats:
			self.__user_stats[author] = AuthorStats()
		
		astats = self.__user_stats[author]

		# Total messages
		astats.n_messages += 1
		# Total characters
		astats.n_chars += len(msg_text)

		# Number of questions
		if '?' in msg_text:
			astats.n_questions += 1

		# 0000 -> 0559: Dawn
		# 0600 -> 1159: Morning
		# 1200 -> 1759: Afternoon
		# 1800 -> 2400: Night
		day_time = 'dawn'
		if msg_date.hour >= 0 and msg_date.hour < 6:
			day_time = astats.DAWN
		elif msg_date.hour >= 6 and msg_date.hour < 12:
			day_time = astats.MORNING
		elif msg_date.hour >= 12 and msg_date.hour < 18:
			day_time = astats.AFTERNOON
		elif msg_date.hour >= 18 and msg_date.hour < 24:
			day_time = astats.NIGHT
		astats.msg_weekday_time[msg_date.weekday()][day_time] += 1

	def export_per_author(self):
		'''
		Output a csv file named after the author, eg. "Aline.txt", 
		with the format "author, count, token"
		'''
		try:
			#f.write('author,count,token\n')
			for author in self.__tokens_for:
				print('Exporting messages from author %s' % author)
				file_name = author.split(' ')[0] + '.txt'
				with codecs.open(file_name, 'w', encoding='utf8') as f:
					for text in self.__tokens_for[author]:
						word = text.replace(',','').replace(':','')
						txt = ('%s:%d\n' % (word, self.__tokens_for[author][text])).strip(' ')
						if len(txt) > 0:
							f.write(txt)
		except IOError as err:
			logger.error('Couldn\'t open or read file. Reason %s' % err.strerror)
			exit(3)

	def export_expanded(self):
		'''
		Expects a Dictionary<Authors Name, Dictionary<Word, Count>> as input
		'''
		try:
			for author in self.__tokens_for:
				file_name = author.split(' ')[0] + '.txt'
				logger.debug('filename %s' % file_name)
				with open(file_name, 'w', encoding='utf8') as f:
					for word in self.__tokens_for[author]:
						logger.debug(self.__tokens_for[author][word])
						txt = (word + ' ') * self.__tokens_for[author][word]
						logger.debug('%d - %s' % (count, word))
						if len(txt) > 1:
							f.write(txt)
		except IOError as err:
			logger.error('Couldn\'t open or read file. Reason %s' % err.strerror)
			exit(3)

	def debug(self):
		for author in self.__tokens_for:
			for text in self.__tokens_for[author]:
				print('%s:::%d:::"%s"' % (author, self.__tokens_for[author][text], text))

def main():
	usage = 'usage: %prog [options]\n'
	parser = OptionParser(usage=usage, version='%prog 1.0')
	(options, args) = parser.parse_args()

	if len(args) == 2:
		logger.info(' input: %s' % args[0])
		logger.info('output: %s' % args[1])
		generator = WordCloudGenerator(args[0])
		generator.process()
		#generator.export(args[1])
		generator.export_per_author()
	else:
		pass
		#generator._tokenizer(txt)
		#generator.debug()


'''
TO-DO:
	* pass
'''

if __name__ == '__main__':
	main()